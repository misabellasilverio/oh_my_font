# Oh My Font Installation Script
# by nongthaihoang @ GitLab

[ -d ${MAGISKTMP:=$(magisk --path)/.magisk} ] && ORIDIR=$MAGISKTMP/mirror
[ -d ${ORIPRD:=$ORIDIR/product} ] || ORIPRD=$ORIDIR/system/product
ORIPRDFONT=$ORIPRD/fonts
ORIPRDETC=$ORIPRD/etc
ORIPRDXML=$ORIPRDETC/fonts_customization.xml
ORISYSFONT=$ORIDIR/system/fonts
ORISYSETC=$ORIDIR/system/etc
ORISYSXML=$ORISYSETC/fonts.xml

PRDFONT=$MODPATH/system/product/fonts
PRDETC=$MODPATH/system/product/etc
PRDXML=$PRDETC/fonts_customization.xml
SYSFONT=$MODPATH/system/fonts
SYSETC=$MODPATH/system/etc
SYSXML=$SYSETC/fonts.xml
MODPROP=$MODPATH/module.prop
mkdir -p $PRDFONT $PRDETC $SYSFONT $SYSETC

FONTS=$MODPATH/fonts
TOOLS=$MODPATH/tools
tar xf $MODPATH/*xz -C $MODPATH
[ -d ${OMFDIR:=/sdcard/OhMyFont} ] || mkdir $OMFDIR

patch() { LD_LIBRARY_PATH=$TOOLS $TOOLS/bspatch $1 $1 $2; }

ver() { sed -i "/version=/s|$|-$1|" $MODPROP; }

xml() { sed -i "$1" $SYSXML; }

uppr() { echo $@ | tr [a-z] [A-Z]; }

src() {
	local i
	for i in $@; do
		local s=$OMFDIR/$i.sh
		[ -f $s ] && ui_print "  Source $i" && \
			. $s && break
	done
}

fcp() {
	local i
	for i in $@; do
		false | cp -i $FONTS/$i $SYSFONT 2>/dev/null
	done
}

romprep() {
	src script_prep && return
	[ -f $ORIPRDFONT/GoogleSans-Regular.ttf ] && \
		cp $ORIPRDXML $PRDXML && \
		PXL=true && return
	cp $ORISYSETC/fonts_slate.xml $SYSETC && \
		OOS=true && return
}

rom() {
	src script && return
	[ $PXL ] && pxl && return
	[ $OOS ] && oos && return
}

pxl() {
	$SANS || GS=true
	[ "${GS:=true}" = true ] && {
		cp $FONTS/G* $PRDFONT; rm $PRDXML
	} || (
	SYSXML=$PRDXML up=$SS it=$SSI fa=google-sans
	[ $GS = $SE ] && up=$SER it=$SERI GS=false
	[ $GS = false ] && {
		ln -s /system/fonts/$up $PRDFONT
		ln -s /system/fonts/$it $PRDFONT
		fontinst r m b
	} )
	ver pxl
}

oos() {
	$SANS && sed -n "$SAF{/$SAQ>/!p}" $SYSXML > tmp && \
		sed -i "$SAF{/$SAQ>/!d};/$SAQ>/r tmp" $SYSETC/fonts_slate.xml
	ver oos
}

comvars() {
	FA=family FAE="/\/$FA/" F=font FE="<\/$F>"
	W=weight S=style I=italic N=normal
	ID=index FF=fallbackFor
	readonly FA FAE F FE W S I N ID FF

	SE=serif
	SA=sans-$SE SAQ="\"$SA\"" SAF="/$SAQ>/,$FAE"
	SC=$SA-condensed
	MO=monospace
	SO=$SE-$MO
	readonly SE SA SAQ SAF SC MO SO

	SF=SFUI.ttf SFI=SFUIItalic.ttf
	SFM=SFUIMono.ttf SFMI=SFUIMonoItalic.ttf
	NY=NewYork.ttf NYI=NewYorkItalic.ttf
	CP=CourierPrime.ttc
	LR=LastResort.ttf
	readonly SF SFI SFM SFMI NY NYI CP LR

	SS=$SF SSI=$SFI SER=$NY SERI=$NYI
	FB=fallback
}

prep() {
	[ -f $ORISYSXML ] || abort "! $ORISYSXML not found"
	comvars; romprep
	! grep -q "$FA >" /system/etc/fonts.xml && {
		find /data/adb/modules/ -type f -name fonts*xml -delete
		false | cp -i /system/etc/fonts.xml $SYSXML && ver '<!>'
	} || false | cp -i $ORISYSXML $SYSXML
	xml '/<!--.*-->/d;/<!--/,/-->/d'
	grep -q "$FA >" $SYSXML && FB=
}

font() {
	local fa=${1:?} f=${2:?} w=${3:-r} s=$N r i
	case $f in *c) i=$ID          ;; esac
	case $w in *s) r=$SE w=${w%?} ;; esac
	case $w in *i) s=$I  w=${w%?} ;; esac
	case $w in
		t ) w=1 ;; el) w=2 ;; l ) w=3 ;;
		r ) w=4 ;; m ) w=5 ;; sb) w=6 ;;
		b ) w=7 ;; eb) w=8 ;; bl) w=9 ;;
	esac
	fa="/$FA.*\"$fa\"/,$FAE" s="${w}00.*$s"
	[ $i ] && s="$s.*$i=\"[0-9]*"
	[ $r ] && s="$s.*$r"; s="$s\">"

	xml "$fa{/$s/s|$FE|\n&|}"
	$axis_del && xml "$fa{/$s/,/$FE/{/$F/!d}}"
	xml "$fa{/$s/s|>.*$|>$f|}"
	[ $4 ] && [ $i ] && \
		xml "$fa{/$s/s|$i=\".*\"|$i=\"$4\"|}" && \
		return

	shift 3 || return; local t v a
	f="$s.*$f" s="/$f/,/$FE/"
	while [ $2 ]; do
		t="tag=\"$1\"" v="stylevalue=\"$2\""
		a="<axis $t $v/>"; shift 2
		xml "$fa{$s{/$t/d};/$f/s|$|\n$a|}"
	done
}

abbr() {
	local n
	case $3 in
		$SF       ) n=u ;;
		$SFI      ) n=i ;;
		$NY|$NYI  ) n=s ;;
		$SFM|$SFMI) n=m ;;
	esac
	[ "$n" = u ] && [ $2 = $SC ] && n=c
	echo $n
}

fontabbr() {
	local w=$2; case $w in *i) w=${2%?} ;; esac
	eval $(echo $1 $2 \$$(uppr `abbr $1`$w))
}

fontinst() {
	local up=$up it=$it cn=$cn ci=$ci i
	[ $up ] && fcp $up && up="font $fa $up"
	[ $it ] && fcp $it && it="font $fa $it"
	[ $cn ] && fcp $cn && cn="font $SC $cn"
	[ $ci ] && fcp $ci && ci="font $SC $ci"
	for i in $@; do
		[ "$up" ] && fontabbr "$up" $i
		[ "$it" ] && fontabbr "$it" ${i}i
		[ "$cn" ] && fontabbr "$cn" $i
		[ "$ci" ] && fontabbr "$ci" ${i}i
	done
}

mksty() {
	case $1 in [a-z]*) local fa=$1; shift ;; esac
	local max=${1:?} min=${2:-1} dw=${3:-1}
	local id=$4 di=${5:-1} fb
	local fae="/$FA.*\"$fa\"/,$FAE"

	$font_del && xml "$fae{/$FA/!d}"; local i=$max j=0
	[ $id ] && j=$id && id=" $ID=\"$j\""
	[ $fallback ] && fb=" $FF=\"$fallback\""
	until [ $i -lt $min ]; do
		for s in $I $N; do
			eval \$$s || continue
			xml "$fae{/$fa/s|$|\n<$F $W=\"${i}00\" $S=\"$s\"$id$fb>$FE|}"
			[ $j -gt 0 ] && j=$(($j-$di)) && id=" $ID=\"$j\""
		done
		[ $i -gt 4 ] && [ $(($i-$dw)) -lt 4 ] && \
			i=4 min=4 || \
			i=$(($i-$dw))
	done
	for i in $wght_del; do xml "$fae{/${i}00/d}"; done
}

fallback() {
	local faq fae fb; [ $1 ] && local fa=$1
	faq="\"$fa\"" fae="/$FA.*$faq/,$FAE"
	[ $fa = $SA ] || fb="/<$F/s|>| $FF=$faq>|;"
	xml "$fae{${fb}H;2,$FAE{${FAE}G}}"
	xml ":a;N;\$!ba;s|name=$faq||2"
}

sfui() {
	local up=$SF it=$SFI cn=$SF fa=$SA

	( up=$FONTS/$up
	case $OTF in *'tailed lowercase L'*         ) otf=l       ;; esac
	case $OTF in *'straight-sided six and nine'*) otf=${otf}s ;; esac
	case $OTF in *'high legibility'*            ) otf=h       ;; esac
	case $OTF in *'tabular figures'*            ) otf=${otf}t ;; esac
	[ $otf ] && patch $up $up.$otf )

	$FB; mksty 9
	fontinst t el l r m sb b eb bl
}

sfmono() {
	local up=$SFM it=$SFMI fa=$MO
	$FB; mksty 8 3
	fontinst l r m sb b eb
}

newyork() {
	local up=$NY it=$NYI fa=$SE
	$FB; mksty 9 4
	fontinst r m sb b eb bl
}

courier() {
	local up=$CP it=$CP fa=$SO
	mksty 7 4 3 3
	fontinst r b
}

lastresort() {
	fcp $LR
	xml "$ i<$FA ><$F $W=\"400\" $S=\"$N\">$LR$FE</$FA>"
}

valof() { sed -n "s|^$1[= ]||p" $UCONF | tail -${2:-1}; }

styof() {
	s=$(valof $1); [ "$s" ] || return
	p=$(sed -n "/^# $s$/{n;s|^# ||
		s|IOPSZ|$(($opsz+1))|
		s|OPSZ|$opsz|
		p}" $UCONF | tail -1)
	[ "$p" ] && echo $p || {
		echo $s | grep -Eq 'wdth|opsz|GRAD|wght|YAXS' && \
		echo $s | sed "
			s|IOPSZ|$(($opsz+1))|
			s|OPSZ|$opsz|" || \
		rm $UCONF
	}
}

config() {
	local dconf dver uver
	dconf=$MODPATH/config.cfg dver=`sed -n '/###/,$p' $dconf`
	UCONF=$OMFDIR/config.cfg uver=`sed -n '/###/,$p' $UCONF`
	[ "$uver" != "$dver" ] && \
		cp $dconf $UCONF && ui_print '  Reset'

	SANS=`valof SANS` MONO=`valof MONO`
	SERF=`valof SERF` SRMO=`valof SRMO`
	LAST=`valof LAST` GS=`valof GS`
	OPSZ=`valof OPSZ` OTF=`valof OTF 3`

	local opsz i
	case $OPSZ in *a) OPSZS=false OPSZ=${OPSZ%?} ;; esac
	for i in T EL L R M SB B EB BL; do
		opsz=$OPSZ
		$OPSZS && case $i in
			T ) opsz=$(($opsz-3)) ;;
			EL) opsz=$(($opsz-2)) ;;
			L ) opsz=$(($opsz-1)) ;;
			M ) opsz=$(($opsz+1)) ;;
			SB) opsz=$(($opsz+2)) ;;
			B ) opsz=$(($opsz+3)) ;;
			EB) opsz=$(($opsz+4)) ;;
			BL) opsz=$(($opsz+5)) ;;
		esac
		eval $(echo U$i=\"`styof U$i`\")
		eval $(echo I$i=\"`styof I$i`\")
		eval $(echo C$i=\"`styof C$i`\")
		eval $(echo M$i=\"`styof M$i`\")
		eval $(echo S$i=\"`styof S$i`\")
		[ ! -f $UCONF ] && config && break
	done
}

finish() {
	find $MODPATH/* -maxdepth 0 ! \( -name 'system' -o -name 'module.prop' \) -exec rm -rf {} \;
	find $MODPATH/* -type d -delete 2>/dev/null
	find $MODPATH/system -type d -exec chmod 755 {} \;
	find $MODPATH/system -type f -exec chmod 644 {} \;
}

install_font() {
	ui_print '- Installing'
	ui_print '+ Prepare'
	prep
	ui_print '+ Configure'
	config
	$SANS && {
		ui_print '+ San Francisco Pro'
		sfui
	}
	$MONO && {
		ui_print '+ San Francisco Mono'
		sfmono
	}
	$SERF && {
		ui_print '+ New York'
		newyork
	}
	$SRMO && {
		ui_print '+ Courier Prime'
		courier
	}
	$LAST && {
		ui_print '+ Last Resort'
		lastresort
	}
	ui_print '+ ROM'
	rom
	finish
}

install_font

restart() {
    local reboot=`valof reboot`
    local omfpath=/data/adb/modules/ohmyfont
    ${reboot:=false} && {
        [ -d $omfpath/system ] || reboot
        local old=`find $omfpath/system -type f -exec basename {} \;`
        local new=`find $MODPATH/system -type f -exec basename {} \;`
        [ "$old" = "$new" ] || reboot
        cp -r $MODPATH/system $omfpath
        setprop ctl.restart zygote
    }
}

trap restart 0
