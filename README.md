# Oh My Font
My favorite fonts for Android

## `Devices`
- **Google Pixel 3a XL** _@_ me  
  Google Stock Android 11
- **OnePlus 7 Pro** _@_ [TotallyAnxious](https://forum.xda-developers.com/member.php?u=5377398)  
  OxygenOS 10 (OOS10)
- **OnePlus 7T Pro** _@_ [r9dzz](https://t.me/r9dzz) / [radz_](https://forum.xda-developers.com/m/radz_.5140419/)  
  OxygenOS 11 (OOS11)
- **Samsung Galaxy J2 Core** _@_ [Jewdah](https://t.me/jewdah0711)  
  Samsung Android Go (SGO)

## `Languages`
- Chinese, Japanese, and Korean (CJK) _@_ [Robert](https://t.me/Robert_Hung)

## `Fonts`

#### `Google Sans` _`(Pixel)`_
- Centered colon for time

#### `San Francisco Pro` _`(sans-serif)`_
- Over one hundred languages using Latin, Greek, and Cyrillic scripts.
- Nine weights — from Thin to Black — in both uprights and italics.
- The Text and Display discrete optical sizes.

#### `San Francisco Mono` _`(monospace)`_
- Over one hundred languages using Latin, Greek, and Cyrillic scripts.
- Six weights — from Light to Heavy — in both uprights and italics.

#### `New York` _`(serif)`_
- Over one hundred languages using Latin, Greek, and Cyrillic scripts.
- Six weights — from Regular to Black — in both uprights and italics.

#### [`Courier Prime`](https://quoteunquoteapps.com/courierprime/) _`(serif-monospace)`_
- Four styles — from Regular to Bold — in both uprights and italics.

#### [`Last Resort`](https://github.com/unicode-org/last-resort-font) _`(fallback)`_
- Users see something more meaningful than a black box or other geometric shape for unrepresentable characters.

#### `Noto CJK` _`(sans-serif & serif)`_
- For Simplified Chinese & Traditional Chinese & Japanese & Korean
- Seven weights — from Thin to Black — Only uprights.
- Optional Plugin [Link](https://gitlab.com/rodert534/oh_my_font_cjk_addition)

## `Configurations`
- Full weights support.
- Lots of predefined styles.
- Design your own style.
- Useful OpenType features.

## `Useful links`
- [Typography - Visual Design - iOS - Human Interface Guidelines - Apple Developer](https://developer.apple.com/design/human-interface-guidelines/ios/visual-design/typography/)
- [Introduction to variable fonts on the web](https://web.dev/variable-fonts/)
- [Variable fonts guide - CSS: Cascading Style Sheets | MDN](https://developer.mozilla.org/en-US/docs/Web/CSS/CSS_Fonts/Variable_Fonts_Guide)

## `Support me ♥️`
- [PayPal](https://paypal.me/nongthaihoang)
